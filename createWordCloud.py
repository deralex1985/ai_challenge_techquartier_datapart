import nltk
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
import pandas as pd

food_news_data = pd.read_csv('FoodNews.csv',  sep=';')
print(food_news_data.head(10))

import matplotlib.pyplot as plt
from wordcloud import WordCloud
from nltk.tokenize import word_tokenize
import string
import re
from nltk import WordNetLemmatizer


def listToString(s):
    # initialize an empty string
    str1 = ""
    # traverse in the string
    for ele in s:
        str1 += ele
        # return string
    return str1


def preprocess(textdata):
    processedText = []
    # Create Lemmatizer and Stemmer.
    wordLemm = WordNetLemmatizer()

    # Defining regex patterns.
    urlPattern = r"((http://)[^ ]*|(https://)[^ ]*|( www\.)[^ ]*)"
    userPattern = '@[^\s]+'
    alphaPattern = "[^a-zA-Z0-9]"
    sequencePattern = r"(.)\1\1+"
    seqReplacePattern = r"\1\1"

    for word in textdata:
        words = word.lower()
        # Replace all URls with 'URL'
        words = re.sub(urlPattern, ' URL', words)
        # Replace @USERNAME to 'USER'.
        words = re.sub(userPattern, ' USER', words)
        # Replace all non alphabets.
        words = re.sub(alphaPattern, " ", words)
        # Replace 3 or more consecutive letters by 2 letter.
        words = re.sub(sequencePattern, seqReplacePattern, words)

        wordList = ''
        for word in words.split():
            # Checking if the word is a stopword.
            # if word not in stopwordlist:
            if len(word) > 1:
                # Lemmatizing the word.
                word = wordLemm.lemmatize(word)
                wordList += (word + ' ')

        processedText.append(wordList)
    return processedText


def showWordCloud(words):
    words = list(words)
    data = words[:1000]
    plt.figure(figsize=(10, 5))
    wc = WordCloud(max_words=1000, width=1600, height=800, collocations=False).generate(" ".join(data))
    plt.imshow(wc)
    plt.show(block=True)
    return wc

food_news_list=food_news_data["Statement"].to_list()
wordFoodNewsString=listToString(food_news_list)
tokenizedFoodWords= word_tokenize(wordFoodNewsString)
processedFoodText=preprocess(tokenizedFoodWords)
wcFood = showWordCloud(processedFoodText)
WordCloud.to_file(wcFood,'FoodNewsWordCloud.png')