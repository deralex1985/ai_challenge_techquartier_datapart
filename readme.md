## Welcome to Dataphile Code Repository

This is part of the TechQuartier AI Challenge. To get startet follow these steps:

1. Setup Environment (Python 3.8 and Pip Package Manager), Install dependencies

2. Run the file getData.py to obtain the data set and to visually inspect it. 
Kindly acknowledge that due to the lack of time we were not able to implement a fully functional webscraping for newsfood.com to generate a more suitable data set for our purposes. 
That is why we illustrate the technological setuo here with politfact.com. 
Hence we have provided a dummy data set which was manually generated from the www.newsfood.com website. 

3.  Generate word cloud for visual inspection of the data.